// -------------------------------------------------------------------------------------------------------------------------------------
// 3. Declare 3 variables without initialization called username,password and role.
let username;
let password;
let role;
// -------------------------------------------------------------------------------------------------------------------------------------
//  - Create a login function which is able to prompt the user to provide their username, password and role.
//  - use prompt() and update the username,password and role global variables with the prompt() returned values.
//  - add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
//  - if it is, show an alert to inform the user that their input should not be empty.

function login(){
    username = prompt("Enter your username:");
    password = prompt("Enter your password:");
    role = prompt("Enter your role:");

    if((username === "" || null) || (password === "" || null) || (role === "" || null)){
        alert("The input should not be empty");
    }

    else{
        switch (role){
            case "admin":
                alert("Welcome back to the class portal, admin!");
                break;

            case "teacher":
                alert("Thank you for logging in, teacher!");
                break;

            case "student":
                alert("Welcome to the class portal, student!");
                break;

            default:
                alert("Role out of range.");
        }
    }
}
login();

// 4. Add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:

//  - if the user's role is admin, show an alert with the following message:

//    - "Welcome back to the class portal, admin!"

//  - if the user's role is teacher, show an alert with the following message:

//    - "Thank you for logging in, teacher!"

//  - if the user's role is a rookie, show an alert with the following message:

//    - "Welcome to the class portal, student!"

//  - if the user's role does not fall under any of the cases, as a default, show a message:

//    - "Role out of range."

// --------------------------------------------------------------------------------------------------------------------------------------
// 5. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.

function checkAverage(num1, num2, num3, num4){
    let average = (num1 + num2 + num3 + num4)/4;
    average = Math.round(average);
    // console.log(average);
    
    if(average < 74){console.log("Hello, student, your average is: " + average + ". The letter equivalent is F");}
    else if((75 <= average) && (average <= 79)){console.log("Hello, student, your average is: " + average + ". The letter equivalent is D");}
    else if((80 <= average) && (average <= 84)){console.log("Hello, student, your average is: " + average + ". The letter equivalent is C");}
    else if((85 <= average) && (average <= 89)){console.log("Hello, student, your average is: " + average + ". The letter equivalent is B");}
    else if((90 <= average) && (average <= 95)){console.log("Hello, student, your average is: " + average + ". The letter equivalent is A");}
    else if(average>96){console.log("Hello, student, your average is: " + average + ". The letter equivalent is A+");}
}


//  - add parameters appropriate to describe the arguments.
//  - create a new function scoped variable called avg.
//  - calculate the average of the 4 number inputs and store it in the variable avg.
//  - research the use of Math.round() and round off the value of the avg variable.
//  - update the avg variable with the use of Math.round()
//  - console.log() the avg variable to check if it is rounding off first.
//  - add an if statement to check if the value of avg is less than or equal to 74.

//    - if it is, show the following message in a console.log():

//      - "Hello, student, your average is . The letter equivalent is F"

//  - add an else if statement to check if the value of avg is greater than or equal to 75 and if avg is less than or equal to 79.

//    - if it is, show the following message in a console.log():

//      - "Hello, student, your average is . The letter equivalent is D"

//  - add an else if statement to check if the value of avg is greater than or equal to 80 and if avg is less than or equal to 84.

//    - if it is, show the following message in a console.log():

//      - "Hello, student, your average is . The letter equivalent is C"

//  - add an else if statement to check if the value of avg is greater than or equal to 85 and if avg is less than or equal to 89.

//    - if it is, show the following message in a console.log():

//      - "Hello, student, your average is . The letter equivalent is B"

//  - add an else if statement to check if the value of avg is greater than or equal to 90 and if avg is less than or equal to 95.

//    - if it is, show the following message in a console.log():

//      - "Hello, student, your average is . The letter equivalent is A"

//  - add an else if statement to check if the value of avg is greater than 96.

//    - if it is, show the following message in a console.log():

//      - "Hello, student, your average is . The letter equivalent is A+"


// 6. Create a gitlab project repository named activity in S18.

// 7. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.

// 8. Add the link in Boodle.